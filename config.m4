dnl $Id$
dnl config.m4 for extension pfinal

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

dnl PHP_ARG_WITH(pfinal, for pfinal support,
dnl Make sure that the comment is aligned:
dnl [  --with-pfinal             Include pfinal support])

dnl Otherwise use enable:

dnl PHP_ARG_ENABLE(pfinal, whether to enable pfinal support,
dnl Make sure that the comment is aligned:
dnl [  --enable-pfinal           Enable pfinal support])

if test "$PHP_PFINAL" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-pfinal -> check with-path
  dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/pfinal.h"  # you most likely want to change this
  dnl if test -r $PHP_PFINAL/$SEARCH_FOR; then # path given as parameter
  dnl   PFINAL_DIR=$PHP_PFINAL
  dnl else # search default path list
  dnl   AC_MSG_CHECKING([for pfinal files in default path])
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       PFINAL_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$PFINAL_DIR"; then
  dnl   AC_MSG_RESULT([not found])
  dnl   AC_MSG_ERROR([Please reinstall the pfinal distribution])
  dnl fi

  dnl # --with-pfinal -> add include path
  dnl PHP_ADD_INCLUDE($PFINAL_DIR/include)

  dnl # --with-pfinal -> check for lib and symbol presence
  dnl LIBNAME=pfinal # you may want to change this
  dnl LIBSYMBOL=pfinal # you most likely want to change this 

  dnl PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
  dnl [
  dnl   PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $PFINAL_DIR/lib, PFINAL_SHARED_LIBADD)
  dnl   AC_DEFINE(HAVE_PFINALLIB,1,[ ])
  dnl ],[
  dnl   AC_MSG_ERROR([wrong pfinal lib version or lib not found])
  dnl ],[
  dnl   -L$PFINAL_DIR/lib -lm
  dnl ])
  dnl
  dnl PHP_SUBST(PFINAL_SHARED_LIBADD)

  PHP_NEW_EXTENSION(pfinal, pfinal.c, $ext_shared)
fi
